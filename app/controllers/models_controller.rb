class ModelsController < ApplicationController
  def index
    #variables
    make = Make.where(webmotors_id: params[:webmotors_make_id]).first
    @models = Model.where(make_id: make.id)


    #search the models
    uri = define_uri("modelos")

    # Make request for Webmotors site


    response = define_response(uri, { marca: params[:webmotors_make_id] })
    models_json = json_parse response

    # Itera no resultado e grava os modelos que ainda não estão persistidas
    models_json.each do |json|
      if Model.where(name: json["Nome"], make_id: make.id).size == 0
        Model.create(make_id: make.id, name: json["Nome"])
      end
    end
  end
end
