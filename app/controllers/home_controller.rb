class HomeController < ApplicationController
  def index
    #variables
    @makes = Make.all

    #search the make
    uri = define_uri("marcas")

    # Make request for Webmotors site
    response = define_response uri
    json = json_parse response

    # Itera no resultado e grava as marcas que ainda não estão persistidas
    json.each do |make_params|
      if Make.where(name: make_params["Nome"]).size == 0
        Make.create(name: make_params["Nome"], webmotors_id: make_params["Id"])
      end
    end
  end
end
