class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception


  #method to define the URI to be used
  def define_uri(path)
    URI("http://www.webmotors.com.br/carro/#{path}")
  end

  #method to define the response with the uri and hash passed
  def define_response(uri, hash = {})
    Net::HTTP.post_form(uri, hash)
  end

  #method to parse the json
  def json_parse(response)
    JSON.parse response.body
  end
end
